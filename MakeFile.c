all:
	gcc -g -Wall servidor.c -o servidor.o
	gcc -g -Wall cliente.c -o cliente.o
        gcc -g -Wall servidor.o cliente.o -o application

clean:
	rm *.o
	rm application
